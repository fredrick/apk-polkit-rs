<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/PolicyKit/1.0/policyconfig.dtd">

<!--
    Copyright (c) 2020 Rasmus Thomsen <oss@cogitri.dev>

    This file is part of apk-polkit (see https://gitlab.alpinelinux.org/Cogitri/apk-polkit).

    apk-polkit is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    apk-polkit is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with apk-polkit.  If not, see <https://www.gnu.org/licenses/>.
-->

<policyconfig>

  <!--
    Policy definitions for APK Polkit, a Polkit helper for the Alpine Package Keeper
  -->

  <vendor>Rasmus Thomsen</vendor>
  <vendor_url>https://gitlab.alpinelinux.org/Cogitri/apk-polkit</vendor_url>
  <icon_name>package-x-generic</icon_name>

  <action id="dev.Cogitri.apkPolkit1.AddPackage">
    <!-- SECURITY:
          - Normal users need admin authentication to install packages
          - Users in the "wheel" group can install packages without authentication
     -->
    <description>Install new package(s)</description>
    <message>Authentication is required to install new package(s)</message>
    <icon_name>package-x-generic</icon_name>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.imply">dev.Cogitri.apkPolkit1.UpgradePackage  dev.Cogitri.apkPolkit1.DeletePackage dev.Cogitri.apkPolkit1.AddRepository dev.Cogitri.apkPolkit1.RemoveRepository</annotate>
  </action>

  <action id="dev.Cogitri.apkPolkit1.UpdateRepositories">
    <!-- SECURITY:
          - Everyone is allowed to update repos, since it doesn't actually
            commit changes to the system (other than an updated repo index)
            and we don't want to spam users with messages about updating
            the repo every half an hour when GNOME Software runs the update.
     -->
    <description>Update the package database</description>
    <message>Authentication is required to update the package database</message>
    <icon_name>package-x-generic</icon_name>
    <defaults>
      <allow_any>yes</allow_any>
      <allow_inactive>yes</allow_inactive>
      <allow_active>yes</allow_active>
    </defaults>
  </action>

  <action id="dev.Cogitri.apkPolkit1.UpgradePackage">
    <!-- SECURITY:
          - Normal users need admin authentication to upgrade packages
          - Users in the "wheel" group can upgrade packages without authentication
     -->
    <description>Upgrade package(s)</description>
    <message>Authentication is required to upgrade package(s)</message>
    <icon_name>package-x-generic</icon_name>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.imply">dev.Cogitri.apkPolkit1.AddPackage dev.Cogitri.apkPolkit1.DeletePackage dev.Cogitri.apkPolkit1.AddRepository dev.Cogitri.apkPolkit1.RemoveRepository</annotate>
  </action>

  <action id="dev.Cogitri.apkPolkit1.DeletePackage">
    <!-- SECURITY:
          - Normal users need admin authentication to delete packages
          - Users in the "wheel" group can delete packages without authentication
     -->
    <description>Delete package(s)</description>
    <message>Authentication is required to delete package(s)</message>
    <icon_name>package-x-generic</icon_name>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.imply">dev.Cogitri.apkPolkit1.AddPackage dev.Cogitri.apkPolkit1.UpgradePackage dev.Cogitri.apkPolkit1.AddRepository dev.Cogitri.apkPolkit1.RemoveRepository</annotate>
  </action>

  <action id="dev.Cogitri.apkPolkit1.AddRepository">
    <!-- SECURITY:
          - Normal users need admin authentication to add a repository
          - Users in the "wheel" group can add a repository without authentication
     -->
    <description>Add a software repository</description>
    <message>Authentication is required to add a software repository</message>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.imply">dev.Cogitri.apkPolkit1.UpgradePackage dev.Cogitri.apkPolkit1.DeletePackage dev.Cogitri.apkPolkit1.RemoveRepository dev.Cogitri.apkPolkit1.AddPackage</annotate>
  </action>

  <action id="dev.Cogitri.apkPolkit1.RemoveRepository">
    <!-- SECURITY:
          - Normal users need admin authentication to remove a repository
          - Users in the "wheel" group can remove a repository without authentication
     -->
    <description>Remove a software repository</description>
    <message>Authentication is required to remove a software repository</message>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin_keep</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.imply">dev.Cogitri.apkPolkit1.UpgradePackage dev.Cogitri.apkPolkit1.DeletePackage dev.Cogitri.apkPolkit1.AddRepository dev.Cogitri.apkPolkit1.AddPackage</annotate>
  </action>
</policyconfig>

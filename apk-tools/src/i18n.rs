/*
    Copyright: Fractal Developers (https://gitlab.gnome.org/GNOME/fractal)
    SPDX-License-Identifier: GPL-3.0-or-later
*/

use gettextrs::{gettext, ngettext};

/* Based on Fractal's  fractal/fractal-gtk/src/i18n.rs, license: GPL-3.0-or-later*/
fn freplace(input: &str, args: &[&str]) -> String {
    let mut parts = input.split("{}");
    let mut output = parts.next().unwrap_or_default().to_string();
    for (p, a) in parts.zip(args.iter()) {
        output += &((*a).to_string() + &p.to_string());
    }
    output
}

pub fn i18n(format: &str) -> String {
    gettext(format)
}

pub fn i18n_f(format: &str, args: &[&str]) -> String {
    let s = gettext(format);
    freplace(&s, args)
}

pub fn ni18n(single: &str, multiple: &str, number: u32) -> String {
    ngettext(single, multiple, number)
}

pub fn ni18n_f(single: &str, multiple: &str, number: u32, args: &[&str]) -> String {
    let s = ngettext(single, multiple, number);
    freplace(&s, args)
}

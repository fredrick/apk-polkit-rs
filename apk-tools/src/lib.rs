/*
    Copyright: 2020 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

#![deny(clippy::all)]
#![warn(clippy::pedantic)]
#![allow(clippy::must_use_candidate)]

pub mod apk_arrays;
pub mod apk_database;
pub mod apk_package;
pub mod apk_repository;
pub mod error;
pub mod i18n;

pub use crate::apk_database::*;
pub use crate::apk_package::*;
pub use crate::apk_repository::*;
pub use crate::error::*;

#!/bin/sh

set -e

# $1 -> @CURRENT_SOURCE_DIR@ as replaced by meson or syntect_plugin_path, we cd into it
# $2 -> Directory to place the finished binaries, meson replaces @OUTDIR@
# $3 -> The final name of the resulting binary, set output: ['name'] in meson.build
# to change it.
# $4 -> passed to cargo --target-dir, .current_build_dir() from meson
# $5 -> Name of the binary created by cargo inside target/
# $6 -> Version of the project

export MESON_SOURCE_ROOT="${1}"
export APK_LIB_PATH="${7}"
export BUILD_MODE="${8}"

# ANSI codes for getting colors and resetting it
RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m'

echo -e \
"
\tapk-polkit Version:\t\t${GREEN}${6}${NO_COLOR}
\tBuildmode:\t\t\t${GREEN}${BUILD_MODE}${NO_COLOR}
"

cd "$1"

echo $APK_LIB_PATH

if [ "${BUILD_MODE}" = "release" ]; then
    cargo build --release --verbose --locked --target-dir "${4}" ${features}
else
    cargo build --verbose --locked --target-dir "${4}" ${features}
fi

# Cargo can place this here if we're crosscompiling
if [ -f "${4}/${RUST_TARGET}/${BUILD_MODE}/${5}" ]; then
    path="${4}/${RUST_TARGET}/${BUILD_MODE}/${5}"
elif [ -f "${4}/${BUILD_MODE}/${5}" ]; then
    path="${4}/${BUILD_MODE}/${5}"
else
    echo "${RED}Can't determine what dir cargo places compiled binaries in! Tried path ${path} ${NO_COLOR}"
    exit 1
fi

cp "${path}" "${2}/${3}"

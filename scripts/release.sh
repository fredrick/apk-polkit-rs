#!/bin/bash -e

if ! [ "$MESON_BUILD_ROOT" ]; then
    echo "This can only be run via meson, exiting!"
    exit 1
fi

PKGVER=$1-$2
DEST=${MESON_BUILD_ROOT}
DIST=$DEST/dist/$PKGVER
SRC=${MESON_SOURCE_ROOT}


cd "${MESON_SOURCE_ROOT}"
mkdir -p "${DIST}"

ginst() {
	cp -rf $@ "${DIST}"
}

ginst \
	Cargo.toml \
	Cargo.lock \
	meson.build \
	meson_options.txt \
	LICENSE \
	README.md \
	scripts \
	apk-polkit-rs \
	apk-tools \
	apk-tools-sys \
	po \
	data \
	subprojects \
	tests

mkdir -p "${DIST}"/.cargo
cargo vendor apk-polkit-rs-vendor | sed 's/^directory = ".*"/directory = "apk-polkit-rs-vendor"/g' > "${DIST}"/.cargo/config
ginst apk-polkit-rs-vendor

cd "${DEST}"/dist
tar cJvf $PKGVER.tar.xz $PKGVER

if type gpg; then
	gpg --armor --detach-sign $PKGVER.tar.xz
	gpg --verify $PKGVER.tar.xz.asc $PKGVER.tar.xz
fi

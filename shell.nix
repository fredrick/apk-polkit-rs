let
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  pkgs = import (fetchTarball("channel:nixpkgs-unstable")) { overlays = [ moz_overlay ]; };
  rustSrc =
    pkgs.latest.rustChannels.stable.rust.override { extensions = [ "rust-src" ]; };
  buildInputs = with pkgs; [ 
    clang_13
    glib
    lld_13
    llvm_13.dev
    llvmPackages_13.libclang
    meson
    ninja
    openssl.dev
    pkg-config
    python3
    rustfmt
    rustSrc
  ];
in pkgs.mkShell {
  buildInputs = buildInputs;

  LIBCLANG_PATH = "${pkgs.llvmPackages_13.libclang.lib}/lib";
  RUST_SRC_PATH = "${rustSrc}/lib/rustlib/src/rust/src";
  RUSTFLAGS="-C linker=clang -C link-arg=--ld-path=${pkgs.lld_13}/bin/ld.lld";
  LD_LIBRARY_PATH = "${pkgs.lib.makeLibraryPath buildInputs}";
  RUST_BACKTRACE = 1;
}
